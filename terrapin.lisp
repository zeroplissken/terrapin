(in-package #:terrapin)

;; the terrapin itself. just a simple struct
(defstruct terp
  (x 0)
  (y 0)
  (angle 0)
  (pen t))

(defun init-terp (&optional (x 0) (y 0) (angle 0) (pen t))
  "make a new terrapin struct, optionally specify <x> <y> <angle> and <pen>."
  (make-terp :x x :y y :angle angle :pen pen))

;; the global terrapin instance
(defparameter *turtle* (init-terp))

(defun to-radians (angle)
  "basic trig shit"
  (* angle (/ pi 180)))

(defun goto (x y &optional (turtle *turtle*))
  "teleport to <x> <y>"
  (setf (terp-x turtle) x)
  (setf (terp-y turtle) y))

(defun toggle-pen (&optional (turtle *turtle*))
  "turn drawing on/off"
  (if (terp-pen turtle)
    (setf (terp-pen turtle) nil)
    (setf (terp-pen turtle) t)))

(defun pen-up (&optional (turtle *turtle*))
  (setf (terp-pen turtle) nil))

(defun pen-down (&optional (turtle *turtle*))
  (setf (terp-pen turtle) t))

(defun right (degrees &optional (turtle *turtle*))
  (setf (terp-angle turtle) (+ (terp-angle turtle) degrees)))

(defun left (degrees &optional (turtle *turtle*))
  (right (- degrees) turtle))

(defun forward (steps &optional (turtle *turtle*))
  "moves terrapin forward <steps> steps, and draws if the pen is down."
  (let ((x (terp-x turtle))
        (y (terp-y turtle))
        (angle (terp-angle turtle)))
    (let ((dest-x x)
          (dest-y y))
      (setf dest-x (+ dest-x (* steps (cos (to-radians angle)))))
      (setf dest-y (+ dest-y (* steps (sin (to-radians angle)))))
      (if (terp-pen turtle)
        (progn
         (v:move-to x y)
         (v:line-to dest-x dest-y)))
      (setf (terp-x turtle) dest-x)
      (setf (terp-y turtle) dest-y)
      (v:stroke))))

(defun back (steps &optional (turtle *turtle*))
  "backin up"
  (forward (- steps) turtle))
      
(defun init (w h &optional (file "terrapin.png"))
  "creates some globals and move the terrapin to the center of the canvas dimensions."
  (defparameter *width* w)
  (defparameter *height* h)
  (defparameter *file* file)
  (setf (terp-x *turtle*) (/ *width* 2))
  (setf (terp-y *turtle*) (/ *height* 2)))

(defun reset-terp (turtle)
  "move the terrapin back to the center of the canvas, resets angle to 0, pen down."
  (setf (terp-x turtle) (/ *width* 2))
  (setf (terp-y turtle) (/ *height* 2))
  (setf (terp-angle turtle) 0)
  (setf (terp-pen) t))

(defmacro terp (&rest body)
  "the basic drawing macro. Must call (init <x> <y>) first. Creates a Vecto canvas with a white background of size *width* *height*."
  (reset-terp *turtle*)
  `(v:with-canvas (:width *width* :height *height*)
    (v:rectangle 0 0 *width* *height*)
    (v:set-rgb-fill 1 1 1)
    (v:fill-path)
    ,@body
    (v:save-png *file*)))

(defun squarepiece (size)
  "draws a segment of <size> and then turns right 90 degrees. Basically just used for making boxes I guess"
  (forward size)
  (right 90))

(defun square (size)
  "makes a square where each side is of <size>."
  (dotimes (i 4)
    (squarepiece size)))

(defun quad (side1 side2)
  "draws a quadrilateral with segment lengths <side1> and <side2>"
  (dotimes (i 2)
    (squarepiece side1)
    (squarepiece side2)))

(defun triangle (size)
  "draws a triangle with segment length of <size>."
  (dotimes (i 3)
    (forward size)
    (right 120)))

(defun arc-r (radius angle)
  "draws a right-moving arc of <radius> and <angle>."
  (let ((conv (/ 360(* 2 pi))))
    (dotimes (i angle)
      (forward (/ radius conv))
      (right 1))))

(defun arc-l (radius angle)
  "draws a left-moving arc of <radius> and <angle>."
  (let ((conv (/ 360 (* 2 pi))))
    (dotimes (i angle)
      (forward (/ radius conv))
      (left 1))))

(defun circle (radius)
  "draws a circle of <radius>."
  (let ((limit-r (/ 360 (* 2 pi))))
    (dotimes (i 360)
      (forward (/ radius limit-r))
      (right 1))))

(defun poly (side angle)
  "draws a polygon where each segment is <side> length and has <angle> between segments. Drawing ends when the polygon has closed."
  (do ((turn 0 (+ turn angle)))
      ((and (not (eq 0 turn)) (eq 0 (mod turn 360))))
    (forward side)
    (right angle)))
