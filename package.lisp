(defpackage #:terrapin
  (:use #:cl)
  (:local-nicknames (#:v #:vecto))
  (:export
   #:init
   #:goto
   #:toggle-pen
   #:pen-up
   #:pen-down
   #:right
   #:left
   #:forward
   #:back
   #:terp
   #:circle
   #:arc-l
   #:arc-r
   #:square
   #:quad
   #:poly
   #:triangle))
